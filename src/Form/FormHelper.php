<?php

namespace Drupal\thbl\Form;

use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\VocabularyInterface;
use Drupal\thbl\Query\QueryManagerInterface;

/**
 * Basic form helper service.
 */
class FormHelper implements FormHelperInterface {

  /**
   * {@inheritdoc}
   */
  protected static function t($string, array $args = [], array $options = []) {
    if (empty($options['context'])) {
      $options['context'] = 'thbl_formhelper';
    }

    // phpcs:ignore
    return t($string, $args, $options);
  }

  /**
   * {@inheritdoc}
   */
  public static function processContentLanguageElement(array $element, FormStateInterface $form_state, array &$form): array {
    if (empty($element['#content_translation_skip_alter']) && \Drupal::currentUser()->hasPermission('administer content translation')) {
      $mode_default = QueryManagerInterface::THBL_MODE_TREE_ALL;
      $form_object = $form_state->getFormObject();
      if ($form_object instanceof EntityFormInterface) {
        $vocabulary = $form_object->getEntity();
        if ($vocabulary instanceof VocabularyInterface) {
          $mode_default = $vocabulary->getThirdPartySetting('thbl', 'mode', QueryManagerInterface::THBL_MODE_TREE_ALL);
        }
      }

      $modes = [
        QueryManagerInterface::THBL_MODE_TREE_ALL => FormHelper::t('Show all'),
        QueryManagerInterface::THBL_MODE_TREE_TRANSLATED_ONLY => FormHelper::t('Show translated only'),
      ];

      $element['thbl_mode'] = [
        '#type' => 'select',
        '#title' => self::t('Translation display mode'),
        '#description' => self::t('Select whether translated terms should be displayed in term listings or all terms (translated and untranslated) terms. Defaults to all.'),
        '#default_value' => $mode_default,
        '#options' => $modes,
        '#weight' => 10,
        '#states' => [
          'visible' => [
            ':input[name="default_language[content_translation]"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $submit_name = isset($form['actions']['save_continue']) ? 'save_continue' : 'submit';
      // Only add the submit handler on the submit button if the #submit
      // property is already available, otherwise this breaks the form submit
      // function.
      if (isset($form['actions'][$submit_name]['#submit'])) {
        $form['actions'][$submit_name]['#submit'][] = [self::class, 'submitContentLanguageElement'];
      }
      else {
        $form['#submit'][] = [self::class, 'submitContentLanguageElement'];
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function submitContentLanguageElement(array $element, FormStateInterface $form_state): void {
    $key = $form_state->get(['content_translation', 'key']);
    $enabled = $form_state->getValue([$key, 'content_translation']);
    $form_object = $form_state->getFormObject();
    if (!($form_object instanceof EntityFormInterface)) {
      return;
    }

    /** @var \Drupal\taxonomy\VocabularyInterface */
    $vocabulary = $form_object->getEntity();
    if (!($vocabulary instanceof VocabularyInterface)) {
      return;
    }

    if ($enabled) {
      $mode = $form_state->getValue([$key, 'thbl_mode']);
      $vocabulary->setThirdPartySetting('thbl', 'mode', $mode);
    }
    else {
      $vocabulary->unsetThirdPartySetting('thbl', 'mode');
    }
    $vocabulary->save();
  }

}
