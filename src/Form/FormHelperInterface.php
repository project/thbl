<?php

namespace Drupal\thbl\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for form helper services.
 */
interface FormHelperInterface {

  /**
   * Process callback to expand the language_configuration form element.
   *
   * @param array $element
   *   Form API element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param array $form
   *   The form array.
   *
   * @return array
   *   Processed language configuration element.
   */
  public static function processContentLanguageElement(array $element, FormStateInterface $form_state, array &$form): array;

  /**
   * Submit callback for the language_configuration form element.
   *
   * @param array $element
   *   Form API element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public static function submitContentLanguageElement(array $element, FormStateInterface $form_state): void;

}
