<?php

namespace Drupal\thbl\Query;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Query manager service.
 */
class QueryManager implements QueryManagerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database, LanguageManagerInterface $language_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function alterTermTreeQuery(SelectInterface &$query, $langcode = NULL, $mode = QueryManagerInterface::THBL_MODE_TREE_ALL): void {
    // There are multiple queries using the tag "taxonomy_term_access" so we
    // need to find the specific query by searching for a special condition.
    // Get current content language if is not set as function argument.
    if (empty($langcode)) {
      $langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    }

    $tags_original = $query->alterTags;
    // Add custom tag so anyone who wants to can alter this specific query.
    $query->addTag(QueryManagerInterface::THBL_TAG_QUERY_TRANSLATABLE);

    // Alter the query dependent on the selected mode.
    switch ($mode) {
      case QueryManagerInterface::THBL_MODE_TREE_ALL:
        $this->queryAlterTermsAll($query, $langcode);
        break;

      case QueryManagerInterface::THBL_MODE_TREE_TRANSLATED_ONLY:
        $this->queryAlterTermsTranslatedOnly($query, $langcode);
        break;
    }

    // Add original tags.
    foreach (array_keys($tags_original) as $tag) {
      $query->addTag($tag);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabularyFromQuery(SelectInterface $query): string {
    /** @var Drupal\Core\Database\Query\ConditionInterface[] $conditions */
    $conditions = &$query->conditions();
    $condition_key = $this->getQueryConditionKey($query, 't.vid');

    if ($condition_key !== FALSE) {
      return $conditions[$condition_key]['value'] ?? '';
    }

    return '';
  }

  /**
   * Alter the given query to get translated terms only.
   *
   * @param Drupal\Core\Database\Query\SelectInterface $query
   *   The query to alter.
   * @param string $langcode
   *   Langcode to filter the query. If set to NULL the current content language
   *   is used (Drupal\Core\Language\LanguageInterface::TYPE_CONTENT).
   *
   * @see \Drupal\thbl\Query\QueryManagerInterface::THBL_MODE_TREE_TRANSLATED_ONLY
   */
  protected function queryAlterTermsTranslatedOnly(SelectInterface &$query, $langcode): void {
    /** @var Drupal\Core\Database\Query\ConditionInterface[] $conditions */
    $conditions = &$query->conditions();
    $condition_key = $this->getQueryConditionKey($query, 't.default_langcode');

    if ($condition_key === FALSE) {
      // Condition for default_langcode does not exists in query so we better
      // stop here.
      return;
    }

    // Make the query distinct so we don't get duplicate results.
    $query->distinct();

    // Replace condition "t.default_langcode" with proper language condition.
    unset($conditions[$condition_key]);
    $query->condition('t.langcode', $langcode);
    $query->condition('p.langcode', $langcode);

    // Add tag.
    $query->addTag('thbl_translatable_translated_only');
  }

  /**
   * Alter the given query to get all terms.
   *
   * @param Drupal\Core\Database\Query\SelectInterface $query
   *   The query to alter.
   * @param string $langcode
   *   Langcode to filter the query. If set to NULL the current content language
   *   is used (Drupal\Core\Language\LanguageInterface::TYPE_CONTENT).
   *
   * @see \Drupal\thbl\Query\QueryManagerInterface::THBL_MODE_TREE_ALL
   */
  protected function queryAlterTermsAll(SelectInterface &$query, $langcode): void {
    /** @var Drupal\Core\Database\Query\ConditionInterface[] $conditions */
    $conditions = $query->conditions();
    $condition_key = $this->getQueryConditionKey($query, 't.default_langcode');

    if ($condition_key === FALSE) {
      // Condition for default_langcode does not exists in query so we better
      // stop here.
      return;
    }

    // Get the vocabulary the query is filtered by.
    $vid = $this->getVocabularyFromQuery($query);

    /** @var \Drupal\taxonomy\TermStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $data_table_name = $storage->getDataTable();

    // Add distinct to the original query.
    $query->distinct();

    // Create a new query to get language-specific items.
    $query_language = $this->database->select($data_table_name, 't_l');
    $query_language->fields('t_l');
    // Remove condition for default_langcode.
    unset($conditions[$condition_key]);
    // Add conditions.
    foreach ($conditions as $condition) {
      if (!is_array($condition)) {
        continue;
      }
      $field_name = $condition['field'];
      if (substr($field_name, 0, 2) === 't.') {
        $field_name = substr($field_name, 2);
      }

      $query_language->condition($field_name, $condition['value'], $condition['operator']);
    }

    // Filter by language.
    $query_language->condition('t_l.langcode', $langcode);
    $query_language->join('taxonomy_term__parent', 'p', 't_l.tid = p.entity_id AND t_l.langcode = p.langcode');
    $query_language->addExpression('parent_target_id', 'parent');

    // Prepare excluding query.
    $query_exclude = $this->database->select($data_table_name, 't_e');
    $query_exclude->distinct()
      ->addField('t_e', 'tid');
    if (!empty($vid)) {
      $query_exclude->condition('t_e.vid', $vid);
    }
    $query_exclude->condition('t_e.langcode', $langcode);

    // Exclude all language-specific results.
    $query->condition('t.tid', $query_exclude, 'NOT IN');

    // Combine both queries.
    $query->union($query_language);

    // Alter the order by fields and remove the table prefix.
    $order_by = &$query->getOrderBy();
    $order_by_new = [];
    foreach ($order_by as $field => $direction) {
      if (substr($field, 0, 2) === 't.') {
        $field = substr($field, 2);
      }
      $order_by_new[$field] = $direction;
    }
    $order_by = $order_by_new;

    // Add tag.
    $query->addTag('thbl_translatable_all');
  }

  /**
   * Check if a field condition exists in a query and return its key.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The query filtered by a single vocabulary.
   * @param string $condition
   *   The condition string to search for.
   * @param string $key
   *   (optional) The key within the conditions array to match against the
   *   condition.
   *
   * @return int|false
   *   The key of the condition within the queries conditions or
   *   <code>FALSE</code> if the condition does not exist.
   */
  protected function getQueryConditionKey(SelectInterface $query, string $condition, string $key = 'field') {
    /** @var Drupal\Core\Database\Query\ConditionInterface[] $conditions */
    $conditions = $query->conditions();
    $condition_exists = array_keys(array_column($conditions, 'field'), $condition, TRUE);

    return isset($condition_exists[0]) ? $condition_exists[0] : FALSE;
  }

}
