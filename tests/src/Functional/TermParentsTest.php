<?php

namespace Drupal\Tests\thbl\Functional;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\TermInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\thbl\Query\QueryManagerInterface;

/**
 * Tests managing language-aware taxonomy parents through the user interface.
 *
 * @group term_hierarchy_by_language
 */
class TermParentsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['thbl'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The entity type being tested.
   *
   * @var string
   */
  protected $entityTypeId = 'taxonomy_term';

  /**
   * The term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The ID of the vocabulary used in this test.
   *
   * @var string
   */
  protected $vocabularyId = 'test_vocabulary';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->setupLanguages();
    $this->setupVocabulary();
    $this->enableTranslation();

    /* @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
    $this->state = $this->container->get('state');

    // Rebuild the container so that the new languages are picked up by services
    // that hold a list of languages.
    $this->rebuildContainer();

    $this->drupalLogin($this->drupalCreateUser([
      'administer taxonomy',
      'administer content translation',
      'create content translations',
      'translate any entity',
    ]));
  }

  /**
   * Adds additional languages.
   */
  protected function setupLanguages(): void {
    $this->langcodes = ['de', 'fr'];
    foreach ($this->langcodes as $langcode) {
      $language = ConfigurableLanguage::createFromLangcode($langcode);
      $language->save();
    }
    array_unshift($this->langcodes, \Drupal::languageManager()->getDefaultLanguage()->getId());
  }

  /**
   * Adds the necessary vocabulary.
   */
  protected function setupVocabulary(): void {
    Vocabulary::create(['vid' => $this->vocabularyId, 'name' => $this->randomMachineName()])->save();
  }

  /**
   * Enables translation for the current entity type and bundle.
   */
  protected function enableTranslation(): void {
    // Enable translation for the used vocabulary.
    \Drupal::service('content_translation.manager')->setEnabled('taxonomy_term', $this->vocabularyId, TRUE);
  }

  /**
   * Prepare the vocabulary for single tests.
   *
   * @param int $mode
   *   The translation display mode.
   */
  protected function prepareVocabulary(int $mode = QueryManagerInterface::THBL_MODE_TREE_ALL): void {
    // Configure the vocabulary to not hide the language selector.
    $this->drupalGet('admin/structure/taxonomy/manage/' . $this->vocabularyId);
    $edit = [
      'default_language[language_alterable]' => TRUE,
    ];

    if ($mode === QueryManagerInterface::THBL_MODE_TREE_TRANSLATED_ONLY) {
      // Set the translation display mode.
      $edit['default_language[thbl_mode]'] = QueryManagerInterface::THBL_MODE_TREE_TRANSLATED_ONLY;
    }

    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('Updated vocabulary');

    $this->drupalGet('admin/structure/taxonomy/manage/' . $this->vocabularyId);
    // Check that the language selector is displayed.
    $this->assertSession()->fieldValueEquals('edit-default-language-language-alterable', TRUE);
  }

  /**
   * Prepare terms used in the tests.
   *
   * @return \Drupal\taxonomy\TermInterface[]
   *   List of created terms.
   */
  protected function prepareTerms(): array {
    $page = $this->getSession()->getPage();
    $terms = [];

    // Create a term without any parents.
    $this->drupalGet("/admin/structure/taxonomy/manage/{$this->vocabularyId}/add");
    $terms['term_1_en'] = $this->submitAddTermForm('Test term 1 (en)');
    // Translate the term.
    $this->drupalGet("/de/taxonomy/term/{$terms['term_1_en']->id()}/translations/add/en/de");
    $this->getSession()->getPage()->fillField('Name', 'Test term 1: translated (en->de)');
    $this->submitForm([], 'Save');

    // Create a term without any parents and with lagcode "de".
    $this->drupalGet("/admin/structure/taxonomy/manage/{$this->vocabularyId}/add");
    $terms['term_1_de'] = $this->submitAddTermForm('Test term 1 (de)', 'de');

    // Create a term without any parents and with langcode "fr".
    $this->drupalGet("/admin/structure/taxonomy/manage/{$this->vocabularyId}/add");
    $terms['term_1_fr'] = $this->submitAddTermForm('Test term 1 (fr)', 'fr');

    // Explicitly selecting <root> should have the same effect as not selecting
    // anything.
    $this->drupalGet("/admin/structure/taxonomy/manage/{$this->vocabularyId}/add");
    $page->selectFieldOption('Parent terms', '<root>');
    $terms['term_2_en'] = $this->submitAddTermForm('Test term 2 (en)');

    // Create two terms with the previously created ones as parents,
    // respectively.
    $this->drupalGet("/admin/structure/taxonomy/manage/{$this->vocabularyId}/add");
    $page->selectFieldOption('Parent terms', 'Test term 1 (en)');
    $terms['term_3_en'] = $this->submitAddTermForm('Test term 3 (en)');
    $this->drupalGet("/admin/structure/taxonomy/manage/{$this->vocabularyId}/add");
    $page->selectFieldOption('Parent terms', 'Test term 2 (en)');
    $terms['term_4_en'] = $this->submitAddTermForm('Test term 4 (en)');

    // Create a term with term 3 as parent.
    $this->drupalGet("/admin/structure/taxonomy/manage/{$this->vocabularyId}/add");
    $page->selectFieldOption('Parent terms', '-Test term 3 (en)');
    $terms['term_5_en'] = $this->submitAddTermForm('Test term 5 (en)');

    return $terms;
  }

  /**
   * Tests specifying parents when creating terms.
   */
  public function testAddWithParents(): void {
    // Prepare the vocabulary.
    $this->prepareVocabulary();

    $this->drupalGet("/admin/structure/taxonomy/manage/{$this->vocabularyId}/add");
    // Check that we have the language selector.
    $this->assertSession()->fieldExists('langcode[0][value]');

    // Prepare terms.
    $terms = $this->prepareTerms();

    // Check term without any parents.
    $expected = [['target_id' => 0]];
    $this->assertEquals($expected, $terms['term_1_en']->get('parent')->getValue());

    // Check term without any parents and with lagcode "de".
    $expected = [['target_id' => 0]];
    $this->assertEquals($expected, $terms['term_1_de']->get('parent')->getValue());

    // Check term without any parents and with langcode "fr".
    $expected = [['target_id' => 0]];
    $this->assertEquals($expected, $terms['term_1_fr']->get('parent')->getValue());

    // Explicitly selecting <root> should have the same effect as not selecting
    // anything.
    $expected = [['target_id' => 0]];
    $this->assertEquals($expected, $terms['term_2_en']->get('parent')->getValue());

    // Check terms having parents.
    $expected = [['target_id' => $terms['term_1_en']->id()]];
    $this->assertEquals($expected, $terms['term_3_en']->get('parent')->getValue());
    $expected = [['target_id' => $terms['term_2_en']->id()]];
    $this->assertEquals($expected, $terms['term_4_en']->get('parent')->getValue());

    // Check term with term 3 as parent.
    $expected = [['target_id' => $terms['term_3_en']->id()]];
    $this->assertEquals($expected, $terms['term_5_en']->get('parent')->getValue());
  }

  /**
   * Tests specifying parents when creating terms.
   */
  public function testTermsVisibleAll(): void {
    // Prepare the vocabulary.
    $this->prepareVocabulary();
    // Prepare terms.
    $this->prepareTerms();

    // Check if all terms are visible.
    $this->drupalGet("admin/structure/taxonomy/manage/{$this->vocabularyId}/overview");
    $this->assertSession()->pageTextContains('Test term 1 (en)');
    $this->assertSession()->pageTextContains('Test term 1 (de)');
    $this->assertSession()->pageTextNotContains('Test term 1: translated (en->de)');
    $this->assertSession()->pageTextContains('Test term 1 (fr)');
    $this->assertSession()->pageTextContains('Test term 2 (en)');

    // Check terms on page with langcode "de".
    $this->drupalGet("de/admin/structure/taxonomy/manage/{$this->vocabularyId}/overview");
    // The translated term should be visible along with all untranslated terms.
    $this->assertSession()->pageTextNotContains('Test term 1 (en)');
    $this->assertSession()->pageTextContains('Test term 1: translated (en->de)');
    $this->assertSession()->pageTextContains('Test term 1 (de)');
    $this->assertSession()->pageTextContains('Test term 1 (fr)');
    $this->assertSession()->pageTextContains('Test term 2 (en)');
  }

  /**
   * Tests specifying parents when creating terms.
   */
  public function testTermsVisibleTranslatedOnly(): void {
    // Prepare the vocabulary.
    $this->prepareVocabulary(QueryManagerInterface::THBL_MODE_TREE_TRANSLATED_ONLY);
    // Prepare terms.
    $this->prepareTerms();

    // Check that only terms in the current language are visible.
    $this->drupalGet("admin/structure/taxonomy/manage/{$this->vocabularyId}/overview");
    $this->assertSession()->pageTextContains('Test term 1 (en)');
    $this->assertSession()->pageTextNotContains('Test term 1 (de)');
    $this->assertSession()->pageTextNotContains('Test term 1 (fr)');
    $this->assertSession()->pageTextContains('Test term 2 (en)');

    // Check terms on page with langcode "de".
    $this->drupalGet("de/admin/structure/taxonomy/manage/{$this->vocabularyId}/overview");
    // The translated term should be visible along with all untranslated terms.
    $this->assertSession()->pageTextNotContains('Test term 1 (en)');
    $this->assertSession()->pageTextContains('Test term 1: translated (en->de)');
    $this->assertSession()->pageTextContains('Test term 1 (de)');
    $this->assertSession()->pageTextNotContains('Test term 1 (fr)');
    $this->assertSession()->pageTextNotContains('Test term 2 (en)');
  }

  /**
   * Creates a term through the user interface and returns it.
   *
   * @param string $name
   *   The name of the term to create.
   * @param string $langcode
   *   The language code of the term. Defaults to 'en'.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   The newly created taxonomy term.
   */
  protected function submitAddTermForm($name, $langcode = 'en'): TermInterface {
    $this->getSession()->getPage()->fillField('Name', $name);
    $this->getSession()->getPage()->fillField('langcode[0][value]', $langcode);

    $this->submitForm([], 'Save');

    $result = $this->termStorage
      ->getQuery()
      ->condition('name', $name)
      ->execute();
    /* @var \Drupal\taxonomy\TermInterface $term_1 */
    $term_1 = $this->termStorage->load(reset($result));
    $this->assertInstanceOf(TermInterface::class, $term_1);
    return $term_1;
  }

  /**
   * Performs tests that edit terms with a single parent.
   *
   * @return \Drupal\taxonomy\TermInterface[]
   *   A list of terms created for testing.
   */
  protected function doTestEditingSingleParent() {
    $terms = [];

    // Create two terms without any parents and english as language.
    $terms['term_1_en'] = $this->createTerm('Test term 1 (en)');
    $this->drupalGet($terms['term_1_en']->toUrl('edit-form'));
    $this->assertParentOption('<root>', TRUE);
    $this->submitForm([], 'Save');
    $this->assertParentsUnchanged($terms['term_1_en']);
    $terms[] = $terms['term_1_en'];

    $term_2_en = $this->createTerm('Test term 2 (en)');
    $this->drupalGet($term_2_en->toUrl('edit-form'));
    $this->assertParentOption('<root>', TRUE);
    $this->assertParentOption('Test term 1 (en)');
    $this->submitForm([], 'Save');
    $this->assertParentsUnchanged($term_2_en);
    $terms[] = $term_2_en;

    // Create two terms with the previously created terms as parents,
    // respectively.
    $term_3_en = $this->createTerm('Test term 3 (en)', [$terms['term_1_en']->id()]);
    $this->drupalGet($term_3_en->toUrl('edit-form'));
    $this->assertParentOption('<root>');
    $this->assertParentOption('Test term 1 (en)', TRUE);
    $this->assertParentOption('Test term 2 (en)');
    $this->submitForm([], 'Save');
    $this->assertParentsUnchanged($term_3_en);
    $terms[] = $term_3_en;

    $term_4_en = $this->createTerm('Test term 4 (en)', [$term_2_en->id()]);
    $this->drupalGet($term_4_en->toUrl('edit-form'));
    $this->assertParentOption('<root>');
    $this->assertParentOption('Test term 1 (en)');
    $this->assertParentOption('-Test term 3 (en)');
    $this->assertParentOption('Test term 2 (en)', TRUE);
    $this->submitForm([], 'Save');
    $this->assertParentsUnchanged($term_4_en);
    $terms[] = $term_4_en;

    // Create a term with term 3 as parent.
    $term_5_en = $this->createTerm('Test term 5 (en)', [$term_3_en->id()]);
    $this->drupalGet($term_5_en->toUrl('edit-form'));
    $this->assertParentOption('<root>');
    $this->assertParentOption('Test term 1 (en)');
    $this->assertParentOption('-Test term 3 (en)', TRUE);
    $this->assertParentOption('Test term 2 (en)');
    $this->assertParentOption('-Test term 4 (en)');
    $this->submitForm([], 'Save');
    $this->assertParentsUnchanged($term_5_en);
    $terms[] = $term_5_en;

    // Create two terms without any parents and german as language.
    $terms['term_1_de'] = $this->createTerm('Test term 1 (de)');
    $this->drupalGet($terms['term_1_de']->toUrl('edit-form'));
    $this->assertParentOption('<root>', TRUE);
    $this->submitForm([], 'Save');
    $this->assertParentsUnchanged($terms['term_1_de']);
    $terms[] = $terms['term_1_de'];

    $term_2_de = $this->createTerm('Test term 2 (en)');
    $this->drupalGet($term_2_de->toUrl('edit-form'));
    $this->assertParentOption('<root>', TRUE);
    $this->assertParentOption('Test term 1 (de)');
    $this->submitForm([], 'Save');
    $this->assertParentsUnchanged($term_2_de);
    $terms[] = $term_2_de;

    return $terms;
  }

  /**
   * Creates a term, saves it and returns it.
   *
   * @param string $name
   *   The name of the term to create.
   * @param int[] $parent_ids
   *   (optional) A list of parent term IDs.
   * @param string $langcode
   *   (optional) The language code of the term. Defaults to 'en'.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   The created term.
   */
  protected function createTerm($name, array $parent_ids = [], $langcode = 'en') {
    /* @var \Drupal\taxonomy\TermInterface $term */
    $term = $this->termStorage->create([
      'name' => $name,
      'vid' => $this->vocabularyId,
      'langcode' => $langcode,
    ]);
    foreach ($parent_ids as $delta => $parent_id) {
      $term->get('parent')->set($delta, ['target_id' => $parent_id]);
    }

    $term->save();

    return $term;
  }

  /**
   * Asserts that an option in the parent form element of terms exists.
   *
   * @param string $option
   *   The label of the parent option.
   * @param bool $selected
   *   (optional) Whether or not the option should be selected. Defaults to
   *   FALSE.
   */
  protected function assertParentOption($option, $selected = FALSE) {
    $option = $this->assertSession()->optionExists('Parent terms', $option);
    if ($selected) {
      $this->assertTrue($option->hasAttribute('selected'));
    }
    else {
      $this->assertFalse($option->hasAttribute('selected'));
    }
  }

  /**
   * Asserts that the parents of the term have not changed after saving.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term to check.
   */
  protected function assertParentsUnchanged(TermInterface $term) {
    $saved_term = $this->termStorage->load($term->id());

    $expected = $term->get('parent')->getValue();
    $this->assertEquals($expected, $saved_term->get('parent')->getValue());
  }

}
