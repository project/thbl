<?php

namespace Drupal\Tests\thbl\Functional;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests vocabulary settings made by thbl through the user interface.
 *
 * @group term_hierarchy_by_language
 */
class VocabularySettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['thbl'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The entity type being tested.
   *
   * @var string
   */
  protected $entityTypeId = 'taxonomy_vocabulary';

  /**
   * The vocabulary storage.
   *
   * @var \Drupal\taxonomy\VocabularyStorageInterface
   */
  protected $vocabularyStorage;

  /**
   * The IDs of the vocabulary used in this test.
   *
   * @var array
   */
  protected $vocabularyIds = [
    'translation_disabled' => 'vocabulary__translation_disabled',
    'mode_1' => 'vocabulary__mode_1',
    'mode_2' => 'vocabulary__mode_2',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->setupLanguages();
    $this->setupVocabularies();
    $this->enableTranslation();

    /* @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');
    $this->vocabularyStorage = $entity_type_manager->getStorage('taxonomy_vocabulary');

    // Rebuild the container so that the new languages are picked up by services
    // that hold a list of languages.
    $this->rebuildContainer();

    $this->drupalLogin($this->drupalCreateUser(['administer taxonomy', 'administer content translation']));
  }

  /**
   * Adds additional languages.
   */
  protected function setupLanguages(): void {
    $this->langcodes = ['de', 'fr'];
    foreach ($this->langcodes as $langcode) {
      $language = ConfigurableLanguage::createFromLangcode($langcode);
      $language->save();
    }
    array_unshift($this->langcodes, \Drupal::languageManager()->getDefaultLanguage()->getId());
  }

  /**
   * Adds the necessary vocabularies.
   */
  protected function setupVocabularies(): void {
    foreach ($this->vocabularyIds as $vid) {
      Vocabulary::create(['vid' => $vid, 'name' => $this->randomMachineName()])->save();
    }
  }

  /**
   * Enables translation for the current entity type and bundle.
   */
  protected function enableTranslation(): void {
    // Enable translation for some vocabularies.
    \Drupal::service('content_translation.manager')->setEnabled('taxonomy_term', $this->vocabularyIds['mode_1'], TRUE);
    \Drupal::service('content_translation.manager')->setEnabled('taxonomy_term', $this->vocabularyIds['mode_2'], TRUE);
  }

  /**
   * Tests thbl modes.
   */
  public function testSetMode(): void {
    // Check that third party setting is not set for this vocabulary.
    /** @var \Drupal\taxonomy\VocabularyInterface $vocabulary */
    $vocabulary = $this->vocabularyStorage->load($this->vocabularyIds['translation_disabled']);
    $this->assertEquals(NULL, $vocabulary->getThirdPartySetting('thbl', 'mode'), '"Translation display mode" is not set');
  }

}
